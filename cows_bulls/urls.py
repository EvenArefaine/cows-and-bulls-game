from cows_bulls.views import GameDetailView
from django.urls import path

urlpatterns = [
    path("<int:pk>/", GameDetailView.as_view(), name ="show_answer"),
]