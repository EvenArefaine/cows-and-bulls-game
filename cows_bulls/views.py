from django.shortcuts import render
from django.views.generic.detail import DetailView
from cows_bulls.models import Game
# Create your views here.
class GameDetailView(DetailView):
    model = Game
    template_name= "cows_bulls/detail.html"
    context_object_name = "game"

