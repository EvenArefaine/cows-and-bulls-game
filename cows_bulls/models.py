from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
import random

random_string1 = str(random.randint(0,9))
random_string2 = str(random.randint(0,9))
random_string3 = str(random.randint(0,9))
random_string4 = str(random.randint(0,9))
class Game(models.Model):
    box_1_answer = models.CharField(max_length = 1, default = random_string1)

    box_2_answer = models.CharField(max_length = 1, default = random_string2)

    box_3_answer = models.CharField(max_length = 1, default = random_string3)

    box_4_answer = models.CharField(max_length = 1, default = random_string4)

    def __str__(self):
        return (self.box_1_answer) + (self.box_2_answer) + (self.box_3_answer) + (self.box_4_answer)


class Guess(models.Model):
    box_1 = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(9),
            MinValueValidator(0),
        ]
    )

    box_2 = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(9),
            MinValueValidator(0),
        ]
    )

    box_3 = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(9),
            MinValueValidator(0),
        ]
    )
    box_4 = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(9),
            MinValueValidator(0),
        ]
    )

    answer = models.ForeignKey("Game", related_name = "guesses", on_delete=models.PROTECT)



    def __str__(self):
        return (self.box_1) + (self.box_2) + (self.box_3) + (self.box_4)
    
