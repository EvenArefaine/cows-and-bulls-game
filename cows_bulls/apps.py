from django.apps import AppConfig


class CowsBullsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cows_bulls'
